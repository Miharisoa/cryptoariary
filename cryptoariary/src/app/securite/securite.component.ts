import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-securite',
  templateUrl: './securite.component.html',
  styleUrls: ['./securite.component.scss'],
})
export class SecuriteComponent implements OnInit {
  item: any = '../../assets/images/items.jpg';
  products: Object[] = [
    {
      id: 1,
      title: 'CoolWallet S',
      price: '99,00 €',
      description:
        'CoolWallet S est un portefeuille digital alliant sécurité et mobilité. Il a redéfini les attentes du monde en matière de portefeuilles digitaux avec la taille d’une carte de crédit, sa flexibilité physique, sa protection imperméable, ainsi que ses mises à jour logicielles et micrologicielles intelligentes. CoolWallet S peut être jumelé avec des appareils iOS et Android. Il supporte actuellement Bitcoin, Bitcoin Cash, Ethereum, les tokens ERC20, Litecoin, Horizen et XRP.',
    },
    {
      id: 2,
      title: 'Ledger Nano X',
      price: '119,00 €',
      description:
        "Le NGRAVE ZERO est le hardware wallet le plus sûr jamais construit. Le ZERO est 100% hors ligne (plus besoin de connexion USB ou autre réseau), il est à l'épreuve des attaques physiques et son micrologiciel sécurisé a obtenu la plus haute certification de sécurité au monde : EAL7.",
    },
    {
      id: 3,
      title: 'Ngrave Zero',
      price: '318,00 €',
      description:
        'CoolWallet S est un portefeuille digital alliant sécurité et mobilité. Il a redéfini les attentes du monde en matière de portefeuilles digitaux avec la taille d’une carte de crédit, sa flexibilité physique, sa protection imperméable, ainsi que ses mises à jour logicielles et micrologicielles intelligentes. CoolWallet S peut être jumelé avec des appareils iOS et Android. Il supporte actuellement Bitcoin, Bitcoin Cash, Ethereum, les tokens ERC20, Litecoin, Horizen et XRP.',
    },
    {
      id: 4,
      title: 'BitBox02',
      price: '109,00 €',
      description:
        'Le BitBox02 est un hardware wallet suisse permettant aux utilisateurs de protéger leurs cryptomonnaies. L’application BitBoxApp qui l’accompagne fournit une solution tout-en-un pour gérer facilement et en toute sécurité vos biens numériques.',
    },
    {
      id: 5,
      title: 'Ngrave Zero',
      price: '318,00 €',
      description:
        'CoolWallet S est un portefeuille digital alliant sécurité et mobilité. Il a redéfini les attentes du monde en matière de portefeuilles digitaux avec la taille d’une carte de crédit, sa flexibilité physique, sa protection imperméable, ainsi que ses mises à jour logicielles et micrologicielles intelligentes. CoolWallet S peut être jumelé avec des appareils iOS et Android. Il supporte actuellement Bitcoin, Bitcoin Cash, Ethereum, les tokens ERC20, Litecoin, Horizen et XRP.',
    },
    {
      id: 6,
      title: 'BitBox02',
      price: '109,00 €',
      description:
        'Le BitBox02 est un hardware wallet suisse permettant aux utilisateurs de protéger leurs cryptomonnaies. L’application BitBoxApp qui l’accompagne fournit une solution tout-en-un pour gérer facilement et en toute sécurité vos biens numériques.',
    },
  ];
  coffres: Object[] = [
    {
      id: 1,
      title: 'Solo Card',
      price: '19,90 €',
      images: '../../assets/images/solo.jpg',
      description:
        "Les Solo Cards ETH de Coinplus matérialisent les clés privées de vos wallets. Ils permettent le stockage d'Ethereum.",
    },
    {
      id: 2,
      title: 'Solo Pro Card',
      price: '50,00 €',
      images: '../../assets/images/solo-pro-card.png',
      description:
        "Les Solo Pro Cards ETH de Coinplus matérialisent les clés privées de vos wallets. Ils permettent le stockage d'Ethereum. La version pro comporte trois cartes afin de permettre une authentification 'multi-sig' (2 cartes sur 3 sont nécessaires pour recouvrir la clé privée du compte",
    },
    {
      id: 3,
      title: 'Solo Bar',
      price: '400,00 €',
      images: '../../assets/images/solo-bar.jpg',
      description:
        'La SOLO BAR est une barre en métal zéroélectronique sur laquelle sont gravés des codes cryptographiques de votre wallets BTC',
    },
    {
      id: 4,
      title: 'Solo Pro Bar',
      price: '1 500,00 €',
      images: '../../assets/images/solo-pro-bar.jpg',
      description:
        "La SOLO PRO BAR est une barre en métal zéroélectronique sur laquelle sont gravés des codes cryptographiques de votre wallets BTC. La version pro comporte trois produits afin de permettre une authentification 'multi-sig' (2 barres sur 3 sont nécessaires pour recouvrir la clé privée du compte).",
    },
  ];
  constructor(private router: Router) {}

  ngOnInit(): void {}
}
