import { Component, OnInit } from '@angular/core';
import { ServicesModule } from '../app/services/services.module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'cryptoariary';
  userName = '';
  constructor(public route: Router,public newService :ServicesModule){
  }
  ngOnInit(){

  }

  connectionTest(){
    let data = (JSON.parse(localStorage.getItem('User')));
    let userlist : any = [{user:'Administrateur',pass: 'Admin'},{user:'Andry',pass:'andry'},{user:'Mihary',pass:'mihary'},{user:'Sandy',pass:'sandy'},{user:'Enzo',pass:'enzo'}];
    (data.length > 0)?'':localStorage.setItem('User',JSON.stringify(userlist)); 
    const userConnected = JSON.parse(localStorage.getItem('UserConnected'));
    if (!this.newService.testUser){
      if(userConnected !== null){
        this.userName = userConnected[0].user;
        return true;
      }
    } else {
      this.userName = (this.newService.testUser)?userConnected[0].user:'';
      return this.newService.testUser;
    }
  }
  logOut(){
    localStorage.removeItem('UserConnected');
    this.newService.connected(false);
    this.route.navigate(['/']);
    console.log()
  }
}
