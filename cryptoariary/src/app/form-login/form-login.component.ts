import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesModule } from '../services/services.module';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss']
})
export class FormLoginComponent implements OnInit {

  isConnexion = true;
  user = '';
  pass = '';

  constructor(public route: Router,private newService :ServicesModule) { }

  ngOnInit(): void {
  }

  switchConnexion () {
    this.isConnexion = !this.isConnexion;
  }

  connection(user,pass){
    let data = (JSON.parse(localStorage.getItem('User')));
    const userlist : any = (data.length>0)?data:[];
    const existe = (userlist.filter(e=>e.user === user && e.pass === pass).length > 0)?true:false;
    if(!existe){
      this.newService.connected(false);
      alert('ce compte n\'existe pas')
    } else {
      localStorage.setItem('UserConnected',JSON.stringify([{user:user}]));
      this.newService.connected(true);
      this.route.navigate(['/']);
    }
  }

  inscription(user,pass){
    let data = (JSON.parse(localStorage.getItem('User')));
    const userlist : any = (data.length>0)?data:[];
    const existe = (userlist.filter(e=>e.user === user).length > 0)?true:false;
    if(existe){
      this.newService.connected(false);
      alert('cet user existe déja');
    } else {
      userlist.push({user:user,pass:pass})
      localStorage.setItem('User',JSON.stringify(userlist));
      localStorage.setItem('UserConnected',JSON.stringify([{user:user}]));
      this.newService.connected(true);
      this.route.navigate(['/']);
    }
    

  }

}
