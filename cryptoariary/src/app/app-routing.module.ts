import { FormLoginComponent } from './form-login/form-login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StakingComponent } from './staking/staking.component';
import { FermeDeMinageComponent } from './ferme-de-minage/ferme-de-minage.component';
import { SecuriteComponent } from './securite/securite.component';
import { HomeComponent } from './home/home.component';
import { CraComponent } from './cra/cra.component';

const routes: Routes = [
  {
    path: 'staking',
    component:StakingComponent
  },
  {
    path:'minage',
    component: FermeDeMinageComponent
  },
  {
    path: 'securite',
    component: SecuriteComponent,
  },
  { path: '', component: HomeComponent },
  { path: 'login', component: FormLoginComponent },
  { path: 'crypto-ariary', component: CraComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
