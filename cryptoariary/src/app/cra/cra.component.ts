import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-cra',
  templateUrl: './cra.component.html',
  styleUrls: ['./cra.component.scss']
})
export class CraComponent implements OnInit {

  doughnutChartLabels: Label[] = ['Pack 2', 'Pack 2', 'Pack 3'];
  doughnutChartData: MultiDataSet = [
    [55, 25, 20]
  ];
  doughnutChartType: ChartType = 'doughnut';

  constructor() {
    
  }

  ngOnInit(): void {
  }

}
