import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FermeDeMinageComponent } from './ferme-de-minage.component';

describe('FermeDeMinageComponent', () => {
  let component: FermeDeMinageComponent;
  let fixture: ComponentFixture<FermeDeMinageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FermeDeMinageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FermeDeMinageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
