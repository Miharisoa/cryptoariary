import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ferme-de-minage',
  templateUrl: './ferme-de-minage.component.html',
  styleUrls: ['./ferme-de-minage.component.scss']
})
export class FermeDeMinageComponent implements OnInit {

  public lorem = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis dignissimos consequuntur necessitatibus nisi? Numquam consectetur adipisci iste corrupti eius odit earum mollitia quis, saepe non, culpa, ratione excepturi incidunt voluptatem.'
  public cards = [
    {
      img: '../../assets/images/product.jpg',
      nomProduit: 'Ferme menage 1',
      consomation: '',
      details: '',
      info: this.lorem,
      prix: 15
    },
    {
      img: '../../assets/images/product2.jpg',
      nomProduit: 'Ferme menage 2',
      consomation: '',
      details: '',
      info: this.lorem,
      prix: 20
    },
    {
      img: '../../assets/images/product3.jpg',
      nomProduit: 'Ferme menage 3',
      consomation: '',
      details: '',
      info: this.lorem,
      prix: 10
    },
    {
      img: '../../assets/images/product.jpg',
      nomProduit: 'Ferme menage 4',
      consomation: '',
      details: '',
      info: this.lorem,
      prix: 15
    }
  ];

  public Notes = [
    {
      title: 'NOTE AVANT D’INVESTIR DANS LE MATÉRIEL DE MINAGE',
      details:"Cher Client, Cher Investisseur, Ces dernières semaines, les demandes concernant les machines de minage ont considérablement augmenté. Ainsi, si vous êtes novices ou que vous ne maitrisez pas tous les aspects de cet environnement, nous vous encourageons vivement à lire ces lignes. Nous sommes entrés depuis quelques semaines dans une phase particulière sur le marché des cryptomonnaies qui a enregistré une hausse violente sur une très courte période. Cette hausse aussi impressionnante que rapide a offert une très forte médiatisation au monde des cryptomonnaies et de nombreuses personnes se sont mises à s’y intéresser. Or dans le cycle d’apprentissage, l’investissement dans le minage de cryptomonnaies intervient assez rapidement et c’est peut-être dans ce cadre-là que vous vous retrouvez à lire ces lignes."
    },
    {
      title:'POINT SUR LA RENTABILITÉ',
      details:"Il faut comprendre que la hausse des marchés à un impact direct sur la rentabilité du mining. En effet, la rentabilité du minage dépend de trois facteurs : - Le cours des cryptomonnaies : évidemment, lorsque le bitcoin est à 15000€, les résultats de rentabilité ne sont pas les mêmes que lorsque les cours se situent à 30000€. - La difficulté de minage : la quantité de bitcoin perçus par le mineur n’est pas fixe, elle dépend de ce qu’on appelle la difficulté de minage (= le nombre de mineurs en activité). Plus il y a de mineurs sur le réseau, plus la part attribuée à chaque mineur va être réduite et inversement. - Le coût de l’électricité : les machines étant énergivores, l’objectif du mineur est de payer son électricité le moins cher possible afin de limiter les charges de son investissement. Parmi ces 3 facteurs, le seul que nous pouvons anticiper avec précision est le facteur électrique. Les deux autres sont variables dans le temps et de ce fait, la rentabilité d’aujourd’hui n’est pas celle de demain. Par cette explication, nous tenons à attirer votre attention sur le fait qu’à l’heure actuelle, la rentabilité du minage est forte, car les cours sont hauts. Mais dans le temps, il y a fort à parier qu’elle s’équilibre, soit par une correction des cours, soit par une hausse de la difficulté. Bien qu’une hausse encore plus forte soit plausible, cette dernière pourrait être éphémère. Aussi, nous rappelons que pour qu’un projet de minage soit viable, le point clé se situe au niveau de l’électricité. Pour un investisseur français qui paye son Kwh au prix moyen en France (0,15€/kwh), le minage de cryptomonnaies n’est pas forcément la formule d’investissement la plus rentable. Nous reviendrons par la suite sur les alternatives possibles."
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
