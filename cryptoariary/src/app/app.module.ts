import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StakingComponent } from './staking/staking.component';
import { FermeDeMinageComponent } from './ferme-de-minage/ferme-de-minage.component';
import { SecuriteComponent } from './securite/securite.component';
import { HomeComponent } from './home/home.component';
import { CardItemComponent } from './card-item/card-item.component';
import { FooterComponent } from './footer/footer.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { ServicesModule } from '../app/services/services.module';
import { CraComponent } from './cra/cra.component';

import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    StakingComponent,
    FermeDeMinageComponent,
    SecuriteComponent,
    HomeComponent,
    CardItemComponent,
    FooterComponent,
    FormLoginComponent,
    CraComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ServicesModule,
    AppRoutingModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
